import { defineConfig } from 'vitepress';
import { OramaPlugin } from '@orama/plugin-vitepress';

import { GitLabSVG } from './assets/gitlab-icon.mts';
import sidebar from './sidebar.mts';
import navbar from './navbar.mts';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'GNOME JavaScript',
  base: process.env.VITEPRESS_BASE_URL ?? '/',
  description: 'A Guide To GNOME JavaScript!',
  srcDir: 'docs',
  outDir: 'public',
  vite: {
    plugins: [
      // @ts-expect-error
      OramaPlugin(),
    ],
  },
  themeConfig: {
    siteTitle: false,
    logo: { light: '/logo.svg', dark: '/logo-dark.svg' },
    // https://vitepress.dev/reference/default-theme-config
    nav: navbar,
    footer: {
      message: 'MIT Licensed | GJS, A GNOME Project',
      copyright: 'Copyright © 2024 gjs.guide contributors',
    },
    sidebar,
    socialLinks: [
      {
        icon: {
          svg: GitLabSVG,
        },
        link: 'https://gitlab.gnome.org/World/javascript/gjs-guide',
      },
    ],
  },
});
