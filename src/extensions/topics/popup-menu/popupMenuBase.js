import St from 'gi://St';

import * as BoxPointer from 'resource:///org/gnome/shell/ui/boxpointer.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';


const sourceActor = new St.Widget();
const menu = new PopupMenu.PopupMenu(sourceActor, 0.0, St.Side.TOP);

// Adding items
const menuItem1 = menu.addAction('Item 1', () => console.log('activated'));

const menuItem2 = new PopupMenu.PopupMenuItem('Item 2');
menu.addMenuItem(menuItem2, 0);

// Moving items
menu.moveMenuItem(menuItem2, 1);

// Opening and closing menus
menu.open(BoxPointer.PopupAnimation.FADE);
menu.close(BoxPointer.PopupAnimation.NONE);

// Removing items
menuItem1.destroy();
menu.removeAll();
