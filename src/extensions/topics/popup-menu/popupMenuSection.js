import St from 'gi://St';

import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';


// Parent Menu
const sourceActor = new St.Widget();
const menu = new PopupMenu.PopupMenu(sourceActor, 0.0, St.Side.TOP);

// Menu Section
const section = new PopupMenu.PopupMenuSection();
section.addAction('Menu Item', () => console.log('activated'));

menu.addMenuItem(section);

