/* GJS's Built-in modules have custom specifiers */
import Cairo from 'cairo';
import System from 'system';


/* Common imports found in `extension.js` */
import Clutter from 'gi://Clutter';
import Meta from 'gi://Meta';
import St from 'gi://St';
import Shell from 'gi://Shell';


/* Common imports found in `prefs.js` */
import Gdk from 'gi://Gdk?version=4.0';
import Gtk from 'gi://Gtk?version=4.0';
import Adw from 'gi://Adw';
