import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';

// Extensions **MAY** construct built-in JavaScript types in the module scope
const FoobarCache = new Map();

// Extensions **MAY** create static data structures in the module scope
const FoobarState = {
    DEFAULT: 0,
    CHANGED: 1,
};

class Foobar {
    state = FoobarState.DEFAULT;
}

let DEFAULT_FOOBAR = null;

export default class ExampleExtension extends Extension {
    constructor(metadata) {
        super(metadata);

        // Extensions **MAY** create and store a reasonable amount of static
        // data during initialization
        this._state = {
            enabled: false,
        };
    }

    enable() {
        // Extensions **MAY** construct instances of classes and assign them
        // to variables in the module scope
        if (DEFAULT_FOOBAR === null)
            DEFAULT_FOOBAR = new Foobar();

        // Extensions **MAY** dynamically store data in the module scope
        for (let i = 0; i < 10; i++)
            FoobarCache.set(`${i}`, new Date());

        this._state.enabled = true;
    }

    disable() {
        // Extensions **MUST** destroy instances of classes assigned to
        // variables in the module scope
        if (DEFAULT_FOOBAR instanceof Foobar)
            DEFAULT_FOOBAR = null;

        // Extensions **MUST** clear dynamically stored data in the module scope
        FoobarCache.clear();

        this._state.enabled = false;
    }
}
