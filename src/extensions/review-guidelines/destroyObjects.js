import St from 'gi://St';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';

export default class ExampleExtension extends Extension {
    enable() {
        this._widget = new St.Widget();
    }

    disable() {
        this._widget?.destroy();
        this._widget = null;
    }
}
