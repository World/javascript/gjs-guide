import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const settings = new Gio.Settings({schema_id: 'guide.gjs.GVariant'});

// Simple types are easy to work with
const boolValue = settings.get_boolean('boolean-setting');
settings.set_boolean('boolean-setting', !boolValue);

const stringValue = settings.get_string('string-setting');
settings.set_string('string-setting', 'a different string');

const strvValue = settings.get_strv('strv-setting');
settings.set_strv('strv-setting', strvValue.concat('three'));

// Complex types can be handled manually
const complexVariant = settings.get_value('complex-setting');
const complexValue = complexVariant.recursiveUnpack();

const newComplexValue = GLib.Variant('(sasa{sa{sv}})', [
    '',
    [],
    {},
]);
settings.set_value('complex-setting', newComplexValue);
