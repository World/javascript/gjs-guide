import GLib from 'gi://GLib';


// Expected output here is: true
const variantBool = GLib.Variant.new_boolean(true);
print(variantBool.unpack());


// Note that unpack() is discarding the string length for us so all we get is
// the value. Expected output here is: "a string"
const variantString = GLib.Variant.new_string('a string');
print(variantString.unpack());


// In this case, unpack() is only unpacking the array, not the strings in it.
// Expected output here is:
//   [object variant of type "s"],[object variant of type "s"]
const variantStrv = GLib.Variant.new_strv(['one', 'two']);
print(variantStrv.unpack());
