import GLib from 'gi://GLib';


// Both of these function create identical GVariant instances
const stringList = ['one', 'two'];
const variantStrv1 = GLib.Variant.new_strv('as', stringList);
const variantStrv2 = new GLib.Variant('as', stringList);

if (variantStrv1.get_type_string() === 'as')
    print('Variant is an array of strings!');

if (variantStrv1.equal(variantStrv2))
    print('Success!');

if (variantStrv1.get_strv().every(value => stringList.includes(value)))
    print('Success!');
