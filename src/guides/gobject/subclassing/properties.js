import GLib from 'gi://GLib';
import GObject from 'gi://GObject';

// #region subclass
const SubclassExample = GObject.registerClass({
    Properties: {
        'example-property': GObject.ParamSpec.string(
            'example-property',
            'Example Property',
            'A read-write string property',
            GObject.ParamFlags.READWRITE,
            null
        ),
    },
}, class SubclassExample extends GObject.Object {
    get example_property() {
        // Implementing the default value manually
        if (this._example_property === undefined)
            this._example_property = null;

        return this._example_property;
    }

    set example_property(value) {
        // Skip emission if the value has not changed
        if (this.example_property === value)
            return;

        // Set the property value before emitting
        this._example_property = value;
        this.notify('example-property');
    }
});
// #endregion subclass

// #region instance
const objectInstance = new SubclassExample({
    example_property: 'construct value',
});
// #endregion instance

// #region property

// Accessing the property
if (objectInstance.example_property === 'construct value')
    objectInstance.example_property = 'new value';

// #endregion property

// #region boolean
GObject.ParamSpec.boolean(
    'boolean-property',
    'Boolean Property',
    'A property holding a true or false value',
    GObject.ParamFlags.READWRITE,
    true);
// #endregion boolean

// #region string
GObject.ParamSpec.string(
    'string-property',
    'String Property',
    'A property holding a string value',
    GObject.ParamFlags.READWRITE,
    'default string');
// #endregion string

// #region double
GObject.ParamSpec.double(
    'number-property',
    'Number Property',
    'A property holding a JavaScript Number',
    GObject.ParamFlags.READWRITE,
    Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER,
    0.0);
// #endregion double

// #region int64
GObject.ParamSpec.int64(
    'int64-property',
    'Int64 Property',
    'A property holding an JavaScript Number',
    GObject.ParamFlags.READWRITE,
    Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER,
    0);
// #endregion int64

// #region object
GObject.ParamSpec.object(
    'object-property',
    'GObject Property',
    'A property holding an object derived from GObject',
    GObject.ParamFlags.READWRITE,
    GObject.Object);
// #endregion object

// #region boxed
GObject.ParamSpec.boxed(
    'boxed-property',
    'GBoxed Property',
    'A property holding a boxed type',
    GObject.ParamFlags.READWRITE,
    GLib.Source);
// #endregion boxed

// #region param_spec_variant
GObject.param_spec_variant(
    'variant-property',
    'GVariant Property',
    'A property holding a GVariant value',
    new GLib.VariantType('as'),
    new GLib.Variant('as', ['one', 'two', 'three']),
    GObject.ParamFlags.READWRITE);
// #endregion param_spec_variant

// #region jsobject
GObject.ParamSpec.jsobject(
    'jsobject-property',
    'JSObject Property',
    'A property holding a JavaScript object',
    GObject.ParamFlags.READWRITE);
// #endregion jsobject

