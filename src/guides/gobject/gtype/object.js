import GObject from 'gi://GObject';
import Gio from 'gi://Gio';

// #region gtype-from-name
const icon = new Gio.ThemedIcon({name: 'dialog-information'});

if (icon.constructor.$gtype === Gio.ThemedIcon.$gtype)
    console.log('These values are exactly equivalent');

if (icon.constructor.$gtype === GObject.type_from_name('GThemedIcon'))
    console.log('These values are exactly equivalent');
// #endregion gtype-from-name

// #region gtype-instanceof
if (icon instanceof Gio.ThemedIcon)
    console.log('instance is a GThemedIcon');

if (icon instanceof GObject.Object && icon instanceof Gio.Icon)
    console.log('instance is a GObject and GIcon');
// #endregion gtype-instanceof

// #region gtype-from-constructor
const listStore = Gio.ListStore.new(Gio.Icon);

const pspec = GObject.ParamSpec.object(
    'gicon',
    'GIcon',
    'A property holding a GIcon',
    GObject.ParamFlags.READWRITE,
    Gio.Icon);
// #endregion gtype-from-constructor
