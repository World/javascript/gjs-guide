import Gdk from 'gi://Gdk?version=4.0';
import Gtk from 'gi://Gtk?version=4.0';


Gtk.init();

// #region signals-handler
const copyLabel = Gtk.Label.new('Lorem Ipsum');

// Connecting a signal
const handlerId = copyLabel.connect('copy-clipboard', label => {
    console.log(`Copied "${label.label}" to clipboard!`);
});

// Disconnecting a signal
if (handlerId)
    copyLabel.disconnect(handlerId);
// #endregion signals-handler

// #region signals-handler-arguments
const selectLabel = Gtk.Label.new('This label has a popup!');

selectLabel.connect('move-cursor', (label, step, count, extendSelection) => {
    if (label === selectLabel)
        console.log('selectLabel emitted the signal!');

    if (step === Gtk.MovementStep.WORDS)
        console.log(`The cursor was moved ${count} word(s)`);

    if (extendSelection)
        console.log('The selection was extended');
});
// #endregion signals-handler-arguments

// #region signals-handler-return
const linkLabel = new Gtk.Label({
    label: '<a href="https://www.gnome.org">GNOME</a>',
    use_markup: true,
});

linkLabel.connect('activate-link', (label, uri) => {
    if (uri.startsWith('file://')) {
        console.log(`Ignoring ${uri}`);
        return true;
    }

    return false;
});
// #endregion signals-handler-return

// #region signals-handler-async
linkLabel.connect('activate-link', (label, uri) => {
    // Do something asynchronous with the signal arguments
    Promise.resolve(uri).catch(logError);

    return true;
});
// #endregion signals-handler-async
