import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


/**
 * This callback will be invoked once the operation has completed, even if
 * it was cancelled.
 *
 * @param {Gio.File} file - the file object
 * @param {Gio.AsyncResult} result - the result of the operation
 */
function loadContentsCb(file, result) {
    try {
        const [length, contents] = file.load_contents_finish(result);

        console.log(`Read ${length} bytes from ${file.get_basename()}`);
    } catch (e) {
        // If the operation was cancelled we probably did it on purpose, in
        // which case we may just want to mute the error
        if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED))
            logError(e, `Reading ${file.get_basename()}`);
    }
}

const file = Gio.File.new_for_path('test-file.txt');

// This is the cancellable we will pass to the asynchronous method. We need to
// hold a reference to this somewhere if we want to cancel it.
const cancellable = new Gio.Cancellable();

// This method passes the file object to a task thread, reads the contents in
// that thread, then invokes loadContentsCb() in the main thread.
file.load_contents_async(GLib.PRIORITY_DEFAULT, cancellable, loadContentsCb);

// Cancel the operation by triggering the cancellable
cancellable.cancel();
