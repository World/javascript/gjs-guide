/* eslint-disable no-unused-vars */

import GObject from 'gi://GObject';
import Gio from 'gi://Gio';


/*
 * Variables and Exports
 */
// #region variable-mutability
const elementCount = 10;
const elements = [];

for (let i = 0; i < elementCount; i++)
    elements.push(i);

for (const element of elements)
    console.log(`Element #${element + 1}`);
// #endregion variable-mutability


// #region variable-exports
export const PUBLIC_CONSTANT = 100;

export const PublicObject = GObject.registerClass(
class PublicObject extends GObject.Object {
    frobnicate() {
    }
});
// #endregion variable-exports


/*
 * Classes and Functions
 */
// #region class-definition
class MyObject {
    frobnicate() {
    }
}

const MySubclass = GObject.registerClass(
class MySubclass extends GObject.Object {
    constructor(params = {}) {
        /* Chain-up with an object of construct properties */
        super(params);
    }

    frobnicate() {
    }
});
// #endregion class-definition


// #region arrow-functions
class MyClock {
    constructor() {
        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });

        this._settings.connect('changed::clock-show-seconds', () => {
            this.showSeconds = this._settings.get_boolean('clock-show-seconds');
        });

        this._settings.connect('changed::clock-show-weekdays',
            this._onShowWeekdaysChanged.bind(this));
    }

    _onShowWeekdaysChanged() {
        this.showWeekdays = this._settings.get_boolean('clock-show-weekdays');
    }
}
// #endregion arrow-functions

