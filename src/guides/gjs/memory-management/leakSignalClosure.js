import Gio from 'gi://Gio';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';


const mySettings = new Gio.Settings({
    schema_id: 'org.gnome.desktop.interface',
});

class MyObject {
    constructor() {
        const agent = {
            'family': 'Bond',
            'given': 'James',
            'codename': '007',
            'licensed': true,
        };

        // Here is where we should have retained the handler ID
        const id = mySettings.connect('changed::licensed', (settings, key) => {
            // Here we are tracing a reference to the `agent` dictionary. Since
            // the signal handler ID is leaked, it is never disconnected and the
            // `agent` dictionary is leaked
            agent.licensed = mySettings.get_boolean(key);
        });
    }
}


export default class ExampleExtension extends Extension {
    enable() {
        this._myObject = new MyObject();
    }

    disable() {
        this._myObject = null;
    }
}
