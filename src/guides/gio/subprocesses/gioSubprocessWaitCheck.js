import Gio from 'gi://Gio';


try {
    const proc = Gio.Subprocess.new(['sleep', '10'],
        Gio.SubprocessFlags.NONE);

    const success = await proc.wait_check_async(null);
    console.log(`The process ${success ? 'succeeded' : 'failed'}`);
} catch (e) {
    logError(e);
}
