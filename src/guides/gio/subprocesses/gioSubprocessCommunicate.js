import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


// This is the process that we'll be running
const script = `
echo "BEGIN";

while read line; do
  echo "$line";
  sleep 1;
done;
`;


/**
 * This function simply writes the current time to `stdin`
 *
 * @param {Gio.InputStream} stdin - the `stdin` stream
 */
async function writeInput(stdin) {
    try {
        const date = new Date().toLocaleString();
        await stdin.write_bytes_async(new GLib.Bytes(`${date}\n`),
            GLib.PRIORITY_DEFAULT, null);

        console.log(`WROTE: ${date}`);
    } catch (e) {
        logError(e);
    }
}

/**
 * Reads a line from `stdout`, then queues another read/write
 *
 * @param {Gio.OutputStream} stdout - the `stdout` stream
 * @param {Gio.DataInputStream} stdin - the `stdin` stream
 */
function readOutput(stdout, stdin) {
    stdout.read_line_async(GLib.PRIORITY_LOW, null, (stream, result) => {
        try {
            const [line] = stream.read_line_finish_utf8(result);

            if (line !== null) {
                console.log(`READ: ${line}`);
                writeInput(stdin);
                readOutput(stdout, stdin);
            }
        } catch (e) {
            logError(e);
        }
    });
}

try {
    const proc = Gio.Subprocess.new(['bash', '-c', script],
        Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE);

    // Get the `stdin`and `stdout` pipes, wrapping `stdout` to make it easier to
    // read lines of text
    const stdinStream = proc.get_stdin_pipe();
    const stdoutStream = new Gio.DataInputStream({
        base_stream: proc.get_stdout_pipe(),
        close_base_stream: true,
    });

    // Start the loop
    readOutput(stdoutStream, stdinStream);
} catch (e) {
    logError(e);
}
