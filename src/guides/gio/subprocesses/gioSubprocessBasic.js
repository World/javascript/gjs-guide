import Gio from 'gi://Gio';


try {
    // The process starts running immediately after this function is called. Any
    // error thrown here will be a result of the process failing to start, not
    // the success or failure of the process itself.
    const proc = Gio.Subprocess.new(
        // The program and command options are passed as a list of arguments
        ['ls', '-l', '/'],

        // The flags control what I/O pipes are opened and how they are directed
        Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
    );

    // Once the process has started, you can end it with `force_exit()`
    proc.force_exit();
} catch (e) {
    logError(e);
}
