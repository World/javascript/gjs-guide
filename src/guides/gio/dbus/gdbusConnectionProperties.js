import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


/*
 * Getting a property value
 */
try {
    const reply = await Gio.DBus.session.call(
        'org.gnome.Shell',
        '/org/gnome/Shell',
        'org.freedesktop.DBus.Properties',
        'Get',
        new GLib.Variant('(ss)', [
            'org.gnome.Shell',
            'ShellVersion',
        ]),
        null,
        Gio.DBusCallFlags.NONE,
        -1,
        null);

    const [version] = reply.recursiveUnpack();

    console.log(`GNOME Shell Version: ${version}`);
} catch (e) {
    if (e instanceof Gio.DBusError)
        Gio.DBusError.strip_remote_error(e);

    logError(e);
}

/*
 * Setting a property value
 */
try {
    await Gio.DBus.session.call(
        'org.gnome.Shell',
        '/org/gnome/Shell',
        'org.freedesktop.DBus.Properties',
        'Set',
        new GLib.Variant('(ssv)', [
            'org.gnome.Shell',
            'OverviewActive',
            GLib.Variant.new_boolean(true),
        ]),
        null,
        Gio.DBusCallFlags.NONE,
        -1,
        null);
} catch (e) {
    if (e instanceof Gio.DBusError)
        Gio.DBusError.strip_remote_error(e);

    logError(e);
}
