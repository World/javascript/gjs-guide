import Gio from 'gi://Gio';


/**
 * Invoked when a connection to a message bus has been obtained.
 *
 * If there is a client waiting for the well-known name to appear on the bus,
 * you probably want to export your interfaces here. This way the interfaces
 * are ready to be used when the client is notified the name has been owned.
 *
 * @param {Gio.DBusConnection} connection - the connection to a message bus
 * @param {string} name - the name that is requested to be owned
 */
function onBusAcquired(connection, name) {
    console.log(`${name}: connection acquired`);
}

/**
 * Invoked when the name is acquired.
 *
 * On the other hand, if you were using something like GDBusObjectManager to
 * watch for interfaces, you could export your interfaces here.
 *
 * @param {Gio.DBusConnection} connection - the connection that acquired the name
 * @param {string} name - the name being owned
 */
function onNameAcquired(connection, name) {
    console.log(`${name}: name acquired`);
}

/**
 * Invoked when the name is lost or @connection has been closed.
 *
 * Typically you won't see this callback invoked, but it might happen if you
 * try to own a name that was already owned by someone else.
 *
 * @param {Gio.DBusConnection|null} connection - the connection on which to
 *     acquire the name, or %null if the connection was disconnected
 * @param {string} name - the name being owned
 */
function onNameLost(connection, name) {
    console.log(`${name}: name lost`);
}

// Just like a signal handler ID, the `Gio.bus_own_name()` function returns a
// unique ID we can use to unown the name when we're done with it.
const ownerId = Gio.bus_own_name(
    Gio.BusType.SESSION,
    'guide.gjs.Test',
    Gio.BusNameOwnerFlags.NONE,
    onBusAcquired,
    onNameAcquired,
    onNameLost);

// Note that `onNameLost()` is NOT invoked when manually unowning a name.
Gio.bus_unown_name(ownerId);
