import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

// #region interface
const interfaceXml = `
<node>
  <interface name="guide.gjs.Test">
    <method name="SimpleMethod"/>
    <method name="ComplexMethod">
      <arg type="s" direction="in" name="input"/>
      <arg type="u" direction="out" name="length"/>
    </method>
    <signal name="TestSignal">
      <arg name="type" type="s"/>
      <arg name="value" type="b"/>
    </signal>
    <property name="ReadOnlyProperty" type="s" access="read"/>
    <property name="ReadWriteProperty" type="b" access="readwrite"/>
  </interface>
</node>`;
// #endregion interface

// #region service
class Service {
    // Properties
    get ReadOnlyProperty() {
        return GLib.Variant.new_string('a string');
    }

    get ReadWriteProperty() {
        if (this._readWriteProperty === undefined)
            return false;

        return this._readWriteProperty;
    }

    set ReadWriteProperty(value) {
        if (this._readWriteProperty === value)
            return;

        this._readWriteProperty = value;
        this._impl.emit_property_changed('ReadWriteProperty',
            GLib.Variant.new_boolean(this.ReadWriteProperty));
    }

    // Methods
    SimpleMethod() {
        console.log('SimpleMethod() invoked');
    }

    ComplexMethod(input) {
        console.log(`ComplexMethod() invoked with '${input}'`);

        return input.length;
    }

    // Signals
    emitTestSignal() {
        this._impl.emit_signal('TestSignal',
            new GLib.Variant('(sb)', ['string', true]));
    }
}
// #endregion service

// #region usage
let serviceInstance = null;
let exportedObject = null;


function onBusAcquired(connection, _name) {
    // Create the class instance, then the D-Bus object
    serviceInstance = new Service();
    exportedObject = Gio.DBusExportedObject.wrapJSObject(interfaceXml,
        serviceInstance);

    // Assign the exported object to the property the class expects, then export
    serviceInstance._impl = exportedObject;
    exportedObject.export(connection, '/guide/gjs/Test');
}

function onNameAcquired(_connection, _name) {
    // Clients will typically start connecting and using your interface now.
}

function onNameLost(_connection, _name) {
    // Well behaved clients will know not to call methods on your interface now
}

const ownerId = Gio.bus_own_name(
    Gio.BusType.SESSION,
    'guide.gjs.Test',
    Gio.BusNameOwnerFlags.NONE,
    onBusAcquired,
    onNameAcquired,
    onNameLost);
// #endregion usage
