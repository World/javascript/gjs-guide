---
title: Port Extensions to GNOME Shell 48
---
# Port Extensions to GNOME Shell 48

## Metadata

::: tip
There were no relevant changes to `metadata.json` in GNOME 48.
:::

## Extension

::: tip
There were no relevant changes to `extension.js` in GNOME 48.
:::

## Preferences

::: tip
There were no relevant changes to `prefs.js` in GNOME 48.
:::

## GNOME Shell

### Custom Logger

For convenience, `ExtensionBase` added the new `getLogger()` method to provide
a better logging experience for extensions. By using the logger class,
you get the extension name as a prefix in the logs.

These are the supported methods, which you can use in the same way
you use the `console` methods:

- `log()`
- `warn()`
- `error()`
- `info()`
- `debug()`
- `assert()`
- `trace()`
- `group()`
- `groupEnd()`

### `InputSourceManager`

The `/ui/status/keyboard.js/InputSourceManager._switchInputSource()` method
now includes an `event` parameter:

| Old                                            | New                                                   |
| ---------------------------------------------- | ----------------------------------------------------- |
| `_switchInputSource(display, window, binding)` | `_switchInputSource(display, window, event, binding)` |

### Keyboard

The `/ui/keyboard.js/Key` box pointer style class name changed from
`keyboard-subkeys` to `keyboard-subkeys-boxpointer`.

Also `/ui/keyboard.js/EmojiSelection` key style class name changed from
`keyboard-hide-symbolic` to `osk-hide-symbolic`.

The following icon names also changed for `/ui/keyboard.js/Keyboard`:

| Old                           | New                      |
| ----------------------------- | ------------------------ |
| `keyboard-caps-lock-symbolic` | `osk-caps-lock-symbolic` |
| `keyboard-shift-symbolic`     | `osk-shift-symbolic`     |

### `Cogl.SnippetHook`

`Cogl.SnippetHook` is exposed in version 45 and later,
use `Cogl.SnippetHook.FRAGMENT` instead of `Shell.SnippetHook.FRAGMENT`.

### `MessageTray`

`/ui/messageTray.js/Notification` now has a new `removeAction()` method.

### `QuickMenuToggle`

`/ui/quickSettings.js/QuickMenuToggle` changed style class names:

| Old                              | New                                    |
| -------------------------------- | -------------------------------------- |
| `quick-menu-toggle`              | `quick-toggle-has-menu`                 |
| `quick-toggle-arrow icon-button` | `quick-toggle-menu-button icon-button` |

The new separator also has the `quick-toggle-separator` style class name.

### `WindowManager`

These methods in `/ui/windowManager.js/WindowManager` now include an `event`
parameter as their third argument:

- `_startSwitcher()`
- `_startA11ySwitcher()`
- `_switchToApplication()`
- `_openNewApplicationWindow()`
- `_showWorkspaceSwitcher()`

### St Widgets Orientation

The `vertical` property of St widgets is deprecated and will be removed in
a future GNOME Shell release (potentially version 50). Replace its usage with
the `orientation` property, using [`Clutter.Orientation`] values.

For example, use `orientation: Clutter.Orientation.VERTICAL`
instead of `vertical: true`.

[`Clutter.Orientation`]: https://gjs-docs.gnome.org/clutter15~15-orientation/

### Time Limit and Break Manager

GNOME Shell 48 includes new
`/misc/breakManager.js` and `/misc/timeLimitsManager.js` files
for break reminders and screen time statistics.

The main instances of the break manager and time limit manager
can be accessed directly in `/ui/main.js/` with:

| Variable Name            | Type                                              | Description                                                       |
| ------------------------ | ------------------------------------------------- | ----------------------------------------------------------------- |
| `breakManager`           | `/misc/breakManager.js/BreakManager`              | Tracks active/inactive time and signals break times.              |
| `breakManagerDispatcher` | `/misc/breakManager.js/BreakDispatcher`           | Converts break status to notify a break event.                    |
| `timeLimitsManager`      | `/misc/timeLimitsManager.js/TimeLimitsManager`    | Tracks active/inactive time and signals daily time limit reached. |
| `timeLimitsDispatcher`   | `/misc/timeLimitsManager.js/TimeLimitsDispatcher` | Converts time limit status to a notify event.                     |

There is also `/ui/shellDBus.js/ScreenTimeDBus` for the screen time D-Bus interface.

### Notifications Per App Group

GNOME Shell 48 introduces grouped notifications, with the following changes:

- `/ui/calendar.js/NotificationMessage` has been moved to `/ui/messageList.js`.

- `/ui/calendar.js/CalendarMessageList` no longer using section list
    and uses `/ui/messageList.js/MessageView` instead.
    `MessageView` is using `message-view` style class name.

- `/ui/mpris.js/MediaMessage` has been moved to `/ui/messageList.js`.

- `/ui/mpris.js/MediaSection` has been renamed to `/ui/mpris.js/MprisSource`.

- The maximum number of notifications per source for
    `/ui/messageTray.js/MAX_NOTIFICATIONS_PER_SOURCE` has been increased
    from 3 to 10.

- The `/ui/messageList.js/MessageListSection` class has been removed and
    replaced by the `NotificationMessageGroup`. This new class provides
    `expand()` and `colapse()` methods for controlling
    the expansion state of the message group. This class emits an
    `expand-toggle-requested` signal for that.

    These are the style class names using by this class:

    | name                          | description                                 |
    | ----------------------------- | ------------------------------------------- |
    | `.message-group-header`       | Box layout for the message group header.    |
    | `.message-notification-group` | Widget for the group expander layout.       |
    | `.message-group-title`        | Label for the message group title.          |
    | `.message-collapse-button`    | Button for collapsing the group's messages. |

- `/ui/messageList.js/NotificationMessageGroup` uses
    `/ui/messageList.js/MessageGroupExpanderLayout` to manage its layout.

## GJS

### `Clutter.Image`

`Clutter.Image` has been removed and you can use [`St.ImageContent`] instead.
There is no need to version check for `St.ImageContent`
as it's already available in GNOME Shell 45 and higher.

`set_bytes()` and `set_data()` in `St.ImageContent` now have a new parameter,
and the first parameter is now a `Cogl.Context`. You can get the instance from
`global.stage.context.get_backend().get_cogl_context()`.

### `Clutter.Stage.get_key_focus()`

`Clutter.Stage.get_key_focus()` now always matches the `key_focus` property.
If no explicit focus is set, it will return `null`, rather than
the `stage` itself.

### `Meta`

These `Meta` functions have been renamed and moved to the new namespace:

| Old                                     | New                                    |
| --------------------------------------- | -------------------------------------- |
| `Meta.disable_unredirect_for_display`   | `Meta.Compositor.disable_unredirect`   |
| `Meta.enable_unredirect_for_display`    | `Meta.Compositor.enable_unredirect`    |
| `Meta.get_top_window_group_for_display` | `Meta.Compositor.get_top_window_group` |
| `Meta.get_window_actors`                | `Meta.Compositor.get_window_actors`    |
| `Meta.get_window_group_for_display`     | `Meta.Compositor.get_window_group`     |
| `Meta.CursorTracker.get_for_display()`  | `global.backend.get_cursor_tracker()`  |

You can get the `Meta.Compositor` using `global.compositor`.

Also, these `Meta` functions have been removed, and you need to use `Clutter` instead:

| Old                                 | New                            |
| ----------------------------------- | ------------------------------ |
| `Meta.get_clutter_debug_flags()`    | `Clutter.get_debug_flags()`    |
| `Meta.add_clutter_debug_flags()`    | `Clutter.add_debug_flags()`    |
| `Meta.remove_clutter_debug_flags()` | `Clutter.remove_debug_flags()` |

[`St.ImageContent`]: https://gjs-docs.gnome.org/st15~15/st.imagecontent
