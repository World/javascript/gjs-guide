---
title: Legacy Documentation
---

# Legacy Documentation

This document is an archive of extension documentation for versions before major
changes to GNOME Shell. For example, how preferences worked before GTK4 and
Adwaita, or how extensions worked before GNOME Shell began using ESModules.

If you are trying to migrate an extension to a newer version of GNOME Shell,
please see the available [Porting Guides](../#porting-guides) instead.

## Imports and Modules

::: tip
The following documentation about modules only applies to GNOME 44 and earlier.
:::

#### Exporting Modules

Larger extensions or extensions with discrete components often separate code
into modules, including GNOME Shell. You can put code to be exported into `.js`
files and import them in `extension.js`, `prefs.js` or each other.

The basic rules of exporting with GJS's import system are that anything defined
with `var` will be exported, while anything defined with `const` or `let` will
NOT be exported.

<<< @/../src/extensions/upgrading/legacy-documentation/importsModulesExporting.js#snippet{js}

#### Importing Modules

If placed in `example@gjs.guide/exampleLib.js` the script above would be
available as `Me.imports.exampleLib`. If it was in a subdirectory, such as
`example@gjs.guide/modules/exampleLib.js`, you would access it as
`Me.imports.modules.exampleLib`.

<<< @/../src/extensions/upgrading/legacy-documentation/importsModules.js{js}

Many of the elements in GNOME Shell like panel buttons, popup menus and
notifications are built from reusable classes and functions, found in modules
like these:

* [`js/misc/extensionUtils.js`][extension-utils]
* [`js/ui/modalDialog.js`][modal-dialog]
* [`js/ui/panelMenu.js`][panel-menu]
* [`js/ui/popupMenu.js`][popup-menu]

You can browse around in the `js/ui/` folder or any other JavaScript file under
`js/` for more code to be reused. Notice the path structure in the links above
and how they compare to the imports below:

```js
const ExtensionUtils = imports.misc.extensionUtils;
const ModalDialog = imports.ui.modalDialog;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
```

[extension-utils]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/misc/extensionUtils.js
[modal-dialog]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/modalDialog.js
[panel-menu]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/panelMenu.js
[popup-menu]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/popupMenu.js

#### Importing Libraries

Extensions can import libraries from the GNOME platform, or any other library
supporting [GObject Introspection][gobject-introspection]. There are also a few
built-in libraries such as [`Cairo`][cairo] and [`Gettext`][gettext] that are
imported differently.

<<< @/../src/extensions/upgrading/legacy-documentation/importsLibraries.js{js}

[gobject-introspection]: https://gi.readthedocs.io/en/latest/index.html
[cairo]: https://gjs-docs.gnome.org/gjs/cairo.md
[gettext]: https://gjs-docs.gnome.org/gjs/gettext.md

## Extension

### GNOME Shell 44

Below are the types, patterns and examples for extensions written for GNOME 44
and earlier.

#### ExtensionMeta

Below is the signature of the meta object (i.e. `ExtensionMeta`) passed to the
`init()` function of extensions:

<<< @/../src/extensions/upgrading/legacy-documentation/extensionMetaLegacy.js{js}

#### Class Pattern

Below is the scaffolding for an extension class:

<<< @/../src/extensions/upgrading/legacy-documentation/extensionClassLegacy.js{js}

#### Module Pattern

Below is the scaffolding for an extension using top-level functions:

<<< @/../src/extensions/upgrading/legacy-documentation/extensionFunctionsLegacy.js{js}

#### Complete Example

Below is a working example of an extension for GNOME Shell 44 or earlier:

<<< @/../src/extensions/upgrading/legacy-documentation/extensionExampleLegacy.js{js}

## Preferences

### GNOME 42

::: tip
This documentation covers preferences for GNOME 42 to GNOME 44, since there
were no breaking changes between those releases.
:::

Below is the scaffolding for extension preferences using Adwaita and GTK4:

<<< @/../src/extensions/upgrading/legacy-documentation/prefs42.js{js}

### GNOME 40

Below is the scaffolding for extension preferences using GTK4:

<<< @/../src/extensions/upgrading/legacy-documentation/prefs40.js{js}

### GNOME 3.x

Below is the scaffolding for extension preferences using GTK3:

<<< @/../src/extensions/upgrading/legacy-documentation/prefs3.js{js}

## Translations

::: tip
See [`ExtensionUtils`](../topics/extension-utils.md) for more complete
documentation of translation functions available before GNOME 45.
:::

### GNOME 41

::: tip
This documentation covers translations for GNOME 41 to GNOME 44, since there
were no breaking changes between those releases.
:::

<<< @/../src/extensions/upgrading/legacy-documentation/translations41.js{10-15,33-46,61-63 js}

### GNOME 40

::: tip
This documentation covers translations for GNOME 40 and earlier, but may not be
accurate for all previous versions of GNOME Shell.
:::

<<< @/../src/extensions/upgrading/legacy-documentation/translations40.js{10-19,37-50,65-67 js}

## Quick Settings

::: tip
This documentation covers Quick Settings for GNOME 43 to GNOME 44, since the
changes between these releases were minor.
:::

There are many complete examples of Quick Settings in GNOME Shell, which can be
referenced in the [`js/ui/status/`] directory.

[`js/ui/status/`]: https://gitlab.gnome.org/GNOME/gnome-shell/tree/44.0/js/ui/status

#### Imports

These are the relevant imports and class instances for Quick Settings:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#imports{js}


#### Basic Toggle

::: warning
Note that in GNOME 44, the `label` property was renamed to `title`. The `label`
property will continue to work, except as a construct property.
:::

Here is an example of a simple on/off toggle, similar to what the Night Light
uses in GNOME Shell:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#basic-toggle{js}


You may also want your extension to show a panel indicator when the feature is
enabled. The `QuickSettings.SystemIndicator` class is used to display an icon
and also manages quick setting items:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#indicator{js}


Since the code for adding the indicator and toggle item is contained in the
`FeatureIndicator` class, the code for the extension is quite simple:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#extension{js}


#### Toggle Menu

::: warning
Note that in GNOME 44, the `label` property was renamed to `title`. The `label`
property will continue to work, except as a construct property.
:::

For features with a few more settings or options, you may want to add a submenu
to the toggle. The `QuickSettings.QuickMenuToggle` includes a built-in
[Popup Menu](../topics/popup-menu.md), that supports the standard menu functions:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#toggle-menu{js}


#### Slider

The quick settings API also comes with a new class for sliders, for settings
like brightness or volume. The `QuickSettings.QuickSlider` class is fairly
straight forward to use:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#slider{js}


When adding the slider to the menu, you will usually want it to span two
columns, like the default volume slider:

```js
// Add the slider to the menu, this time passing `2` as the second
// argument to ensure the slider spans both columns of the menu
QuickSettingsMenu._addItems([new FeatureSlider()], 2);
```

#### Action Button

It's also possible to add action buttons to the top of the quick settings, such
as the *Lock Screen* or *Settings* button. Note that this is a very prominent
location in the UI with limited space, so you should consider carefully before
adding more buttons here.

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#action-button{js}


#### Menu Placement

GNOME Shell 44 features a new *Background Apps* menu in the quick settings menu,
which looks different from the other tiles. If you want your toggle placed above
the *Background Apps* menu (or any other widget), you can move it after adding
it with the built-in function:

<<< @/../src/extensions/upgrading/legacy-documentation/quickSettings44.js#menu-placement{js}



## Search Provider

::: tip
This documentation covers Search Providers for GNOME 43 to GNOME 44, since there
were no breaking changes between those releases.
:::

A search provider is a mechanism by which an application can expose its search
capabilities to GNOME Shell. Text from the search entry is forwarded to all
search providers, which may each return a set of search results.

#### Imports

These are the relevant imports and class instances for Search Providers:

<<< @/../src/extensions/upgrading/legacy-documentation/searchProvider43.js#imports{js}


#### `ResultMeta`

The `ResultMeta` object is a light-weight metadata object, used to represent a
search result in the search view. Search providers must return objects of this
type when `SearchProvider.prototype.getResultMetas()` is called.

<<< @/../src/extensions/upgrading/legacy-documentation/searchProvider43.js#result-meta{js}


The `id` is the result identifier, as returned by the provider.

The `name` property holds a name or short description of the result.

The `description` property is optional, holding a longer description of the
result that is only displayed in the list view.

The `clipboardText` property is optional, holding text that will be copied to
the clipboard if the result is activated.

The `createIcon` property holds a function that takes a size argument and
returns a `Clutter.Actor`, usually an `St.Icon`:

```js
/**
 * Create an icon for a search result.
 *
 * Implementations may want to take scaling into consideration.
 *
 * @param {number} size - The requested size of the icon
 * @returns {Clutter.Actor} An icon
 */
function createIcon(size) {
    const { scaleFactor } = St.ThemeContext.get_for_stage(global.stage);

    return new St.Icon({
        icon_name: 'dialog-question',
        width: size * scaleFactor,
        height: size * scaleFactor,
    });
}
```

#### Example Provider

GNOME Shell extensions create search providers by creating a class implementing
a [simple interface](#searchprovider). This class is responsible for returning a
list of results for a list of search terms.

Results are returned as unique string identifiers, which may be passed back to
the search provider to request [`ResultMeta`](#resultmeta) objects. These are
used by GNOME Shell to populate the results displayed to the user.

Search providers are constructed and then [registered](#registration) with GNOME
Shell, before they start receiving search requests.

<<< @/../src/extensions/upgrading/legacy-documentation/searchProvider43.js#search-provider{js}


Search providers from GNOME Shell extensions must be registered before they
become active. Registration should be performed in the `enable()` function and
then later unregistered in `disable()`.

<<< @/../src/extensions/upgrading/legacy-documentation/searchProvider43.js#extension{js}



## Session Modes

::: tip
This documentation covers Session Modes for GNOME 42 to GNOME 44, since there
were no breaking changes between those releases.
:::

Session modes are environment states of GNOME Shell. For example, when a user is
logged in and using their desktop the Shell is in the `user` mode.

Since GNOME 42, extensions have the option of operating in other session modes,
such as the `unlock-dialog` mode when the screen is locked. For more details,
see the [`session-modes`](/extensions/overview/anatomy.html#session-modes)
documentation.

## Example Usage

Here is an example of a `metadata.json` for an extension that can run in the
regular `user` mode and continue running in the `unlock-dialog` mode, when the
screen is locked. Pay attention that the shell may use custom user modes that
are not named `user`, so we need to ensure this by also checking the parent mode.

```json
{
    "uuid": "session-modes@gjs.guide",
    "name": "Session Modes Example",
    "description": "This is an example of using session modes in an extension.",
    "shell-version": [ "42" ],
    "session-modes": ["user", "unlock-dialog"],
    "url": "https://gjs.guide/extensions/"
}
```

Like standard extensions, the extension will be enabled when the user logs in
and logs out, but won't be disabled when the screen locks.

Extensions that continue running on the lock screen will usually want to disable
UI elements when the session is locked, while continuing to operate in the
background.

<<< @/../src/extensions/upgrading/legacy-documentation/sessionModes42.js{js}
