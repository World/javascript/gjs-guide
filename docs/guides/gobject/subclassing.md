---
title: Subclasses
---

# GObject Subclassing

Subclassing is a convenient way to extend most GObject classes, allowing you to
define additional methods, properties and signals. If you haven't read the
[GObject Basics](basics.md) guide already some of the concepts in this guide
may be unfamiliar to you, so consider reading that first.

## Subclassing GObject

::: warning
Note that prior to GJS 1.72 (GNOME 42), it was required to override `_init()`
and chain-up with `super._init()` instead of the standard `constructor()`.
:::

Every class of GObject has a globally unique [GType](#gtype) and so each
subclass must be registered using the `GObject.registerClass()` function. This
function takes a dictionary of GObject attributes as the first argument, and a
JavaScript class declaration as its second argument.

Below is an example of a GObject class declaration with a
[`GTypeName`](#gtype-name), [`Properties`](#properties) and [`Signals`](#signals)
defined.

<<< @/../src/guides/gobject/subclassing/object.js{js}

## GTypeName

::: tip
See the [`GType`](gtype.md#gtype-name) guide for detailed information about the
value of this field.
:::

By default, the `GType` name of a subclass in GJS will be the class name
prefixed with `Gjs_`. Usually setting a custom name is not necessary unless you
need to refer to the type by name, such as in a `GtkBuilder` interface
definition.

To specify a custom `GType` name, you can pass it as the value for the
`GTypeName` property to `GObject.registerClass()`:

<<< @/../src/guides/gobject/subclassing/gtype.js#snippet{js}


## Properties

::: tip
See the [GObject Basics](basics.md#properties) guide for an introduction to
how to use properties in GJS.
:::

### Declaring Properties

When defining properties of a GObject subclass, the properties must be declared
in the `Properties` dictionary of the class definition. Each entry contains a
[`GObject.ParamSpec`] defining the attributes and behavior of the property,
while the `get` and `set` accessors control the value.

<<< @/../src/guides/gobject/subclassing/properties.js#subclass{2-10 js}

The class defined above can then be constructed with a dictionary of the
declared properties:

<<< @/../src/guides/gobject/subclassing/properties.js#instance{js:no-line-numbers}


[`GObject.ParamSpec`]: https://gjs-docs.gnome.org/gobject20/gobject.paramspec

### Property Types

`Boolean` and `String` properties are the simplest of properties. The default
value for a `Boolean` should be `true` or `false`, but a `String` property may
have a `null` default.

<<< @/../src/guides/gobject/subclassing/properties.js#boolean{js:no-line-numbers}

<<< @/../src/guides/gobject/subclassing/properties.js#string{js:no-line-numbers}

#### Numeric Types

::: warning
The 64-bit numeric types can not hold the full value range in GJS. See the
[upstream issue](https://gitlab.gnome.org/GNOME/gjs/issues/271) for details.
:::

Properties with numeric types have additional parameters for the value range.
The most commonly used in GJS classes are `GObject.ParamSpec.double()`,
`GObject.ParamSpec.int()` and `GObject.ParamSpec.uint()`.

<<< @/../src/guides/gobject/subclassing/properties.js#double{js:no-line-numbers}


The GType `GObject.TYPE_DOUBLE` is equivalent to the JavaScript [`Number`] type,
and can be fully represented by `GObject.ParamSpec.double()`. The 64-bit types
such as `GObject.TYPE_INT64` are not mapped to [`BigInt`], which limits
`GObject.ParamSpec.int64()` to the range of [`Number.MIN_SAFE_INTEGER`] and
[`Number.MAX_SAFE_INTEGER`].

<<< @/../src/guides/gobject/subclassing/properties.js#int64{js:no-line-numbers}


[`BigInt`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/BigInt
[`Number`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number
[`Number.MIN_SAFE_INTEGER`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/MIN_SAFE_INTEGER
[`Number.MAX_SAFE_INTEGER`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER

#### Complex Types

::: tip
Some libraries define their own types, such as [`Gtk.param_spec_expression()`].
:::

There are several `GObject.ParamSpec` types for complex values, the most
common being for GObject and GBoxed. Both of these require a `GType`, although
`GObject.ParamSpec.object()` can be passed an super-class of the expected type.

<<< @/../src/guides/gobject/subclassing/properties.js#object{js:no-line-numbers}


The `GObject.ParamSpec` for a [`GLib.Variant`] expects a [`GLib.VariantType`]
describing the type of value it will hold.

<<< @/../src/guides/gobject/subclassing/properties.js#param_spec_variant{js:no-line-numbers}


There is also support for JavaScript types that derive from the native
[`Object`] type. This includes `Object`, `Array` and more complex types like
`Date()`.

<<< @/../src/guides/gobject/subclassing/properties.js#jsobject{js:no-line-numbers}


[`GLib.Variant`]: https://gjs-docs.gnome.org/glib20/glib.variant
[`GLib.VariantType`]: https://gjs-docs.gnome.org/glib20/glib.varianttype
[`Gtk.param_spec_expression()`]: https://gjs-docs.gnome.org/gtk40/gtk.param_spec_expression
[`Object`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

### Property Flags

GObject properties are not only strictly typed, but also have with restrictions
on if they are read-only, write-only, read-write or changeable after
construction. This behavior is controlled by the [`GObject.ParamFlags`]. Below
are the most commonly used flags:

* `GObject.ParamFlags.READABLE`

  A property with this flag can be read.

* `GObject.ParamFlags.WRITABLE`

  A property with this flag is written. Write-only properties are rarely used.

* `GObject.ParamFlags.READWRITE`

  This is an alias for `GObject.ParamFlags.READABLE | GObject.ParamFlags.WRITABLE`.

* `GObject.ParamFlags.CONSTRUCT_ONLY`

  A property with this flag can only be written during construction.


[`GObject.ParamFlags`]: https://gjs-docs.gnome.org/gobject20/gobject.paramflags

### Property Change Notification

As introduced in the [GObject Basics](basics.md) guide, all GObjects have a
[`notify`] signal that may be emitted when a property changes. GObject
subclasses in GJS must explicitly emit this signal for properties by calling
[`GObject.Object.notify()`].

<<< @/../src/guides/gobject/subclassing/properties.js#subclass{27 js}

[`GObject.Object.notify()`]: https://gjs-docs.gnome.org/gobject20/gobject.object#method-notify
[`notify`]: https://gjs-docs.gnome.org/gobject20/gobject.object#signal-notify

## Signals

::: tip
See the [GObject Basics](basics.md#signals) guide for an introduction to
how to use signals in GJS.
:::

### Declaring Signals

When defining signals of a GObject subclass, the signals must be declared in
the `Signals` dictionary of the class definition. The simplest signals with the
default behavior only require a name.

<<< @/../src/guides/gobject/subclassing/signals.js#example{2-4 js}

Callbacks are connected as handlers for the signal, like with any other GObject
class:

<<< @/../src/guides/gobject/subclassing/signals.js#connection{js:no-line-numbers}


A default signal handler can be defined in the class, and the following
attributes can also be changed in the signal declaration.

| Key           | Default                         | Description                |
|---------------|---------------------------------|----------------------------|
| `flags`       | `GObject.SignalFlags.RUN_FIRST` | Emission behavior          |
| `param_types` | `[]` (No arguments)             | List of `GType` arguments  |
| `return_type` | `GObject.TYPE_NONE`             | Return type of callbacks   |
| `accumulator` | `GObject.AccumulatorType.NONE`  | Return value behavior      |

### Default Handler

::: tip
The signal emission phases are described in the official [Signals Documentation].
:::

Classes can set a default handler for a signal and subclasses can override them.
A default handler is set by defining a class method prefixed with `on_`, such
as `on_example_handler()` for the signal `example-handler`.

<<< @/../src/guides/gobject/subclassing/signals.js#defaults{4,8-10 js}

The default handler for a signal is always invoked, regardless of whether a
user handler (i.e. callback) is connected to the signal. The order the default
handler is invoked is controlled by whether it has the flag `RUN_FIRST`,
`RUN_LAST` or `RUN_CLEANUP`.

<<< @/../src/guides/gobject/subclassing/signals.js#emit-example{js:no-line-numbers}


[Signals Documentation]: https://docs.gtk.org/gobject/concepts.html#signal-emission

### Signal Flags

Signal flags can control several aspects of the emission, the most commonly
used are below:

* `GObject.SignalFlags.RUN_FIRST`
* `GObject.SignalFlags.RUN_LAST`
* `GObject.SignalFlags.RUN_CLEANUP`

  As explained above, these three flags determine which emission phase the
  default handler will be invoked.

* `GObject.SignalFlags.DETAILED`

  A signal with this flag allows signal to be emitted with a detail string. For
  example, the GObject signal `notify` can be emitted with a property name as a
  detail.

The [`GObject.SignalFlags`] enumeration describes all the possible flags for
signals.

<<< @/../src/guides/gobject/subclassing/signals.js#detailed{3-5,8-10 js}

The signal above can be connected to with an optional "detail" appended to the
signal name. In that case, the handler will only be run if the emission detail
matches the handler detail.

Since the `RUN_LAST` flag is used, the default handler will run after a user
handler connected with `GObject.Object.connect()`, but before a user handler
connected with `GObject.Object.connect_after()`.

<<< @/../src/guides/gobject/subclassing/signals.js#detailed-connect{js:no-line-numbers}


[`GObject.SignalFlags`]: https://gjs-docs.gnome.org/gobject20/gobject.signalflags

### Signal Parameters

The first argument for a signal callback is always the emitting object, but
additional parameters can also be defined for signals using the `param_types`
key:

<<< @/../src/guides/gobject/subclassing/signals.js#parameters{3-5 js}

Callbacks then receive the additional parameter value as function arguments:

<<< @/../src/guides/gobject/subclassing/signals.js#parameters-connect{109-116 js:no-line-numbers}


### Signal Return Values

Signals may be configured to require a return value from handlers, allowing a
callback to communicate to the emitting object. In most cases this is a
`boolean` value, but other types are possible.

<<< @/../src/guides/gobject/subclassing/signals.js#rvals{3-5 js}

Callbacks for the signal should return an appropriate value, which the emitting
object can act on:

<<< @/../src/guides/gobject/subclassing/signals.js#rvals-connect{js:no-line-numbers}



### Signal Accumulator

Signal accumulators are special functions that collect the return values of
callbacks, similar to how [`reduce()`] works. Currently GJS only supports two
built-in accumulators:

* `GObject.AccumulatorType.NONE`

  This is the default.

* `GObject.AccumulatorType.FIRST_WINS`

  This accumulator will use the return value of the first handler that is run.
  A signal with this accumulator may have a return of any type.

* `GObject.AccumulatorType.TRUE_HANDLED`

  This accumulator will stop emitting once a handler returns `true`. A signal
  with this accumulator must have a return type of `GObject.TYPE_BOOLEAN`.

Below is an example of declaring a signal with the `TRUE_HANDLED` accumulator
that stops signal emission after the second user callback returns `true`.

<<< @/../src/guides/gobject/subclassing/signals.js#accumulator{3-7 js}

It can be seen that when emitting the signal, the first connected handler that
returns `true` prevents later user handlers and the default handler from
running:

<<< @/../src/guides/gobject/subclassing/signals.js#accumulator-connect{ js:no-line-numbers}


[`reduce()`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce

